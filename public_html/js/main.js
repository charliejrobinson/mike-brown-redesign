/**
 * Created by charlie on 11/12/2014.
 */

$(document).ready(function() {

    $(".mob-menu").click(function() {
        $('ol').toggleClass("active");

    });


    $(window).scroll(function() {    

    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".logo").addClass("logo-scroll");
        $("nav").addClass("nav-scroll");
        $(".tel").addClass("tel-scroll");
    } else {

        $(".logo").removeClass("logo-scroll");
        $("nav").removeClass("nav-scroll");
        $(".tel").removeClass("tel-scroll");

    }
});


});



/*
 Or with jQuery/Zepto
 */
$(function(){
    var mySwiper = $('.swiper-container').swiper({
        //Your options here:
        mode:'horizontal',
        loop: true,
        autoplay: 4000
        //etc..
    });
})